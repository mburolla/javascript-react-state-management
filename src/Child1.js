import React from 'react'
import { useContext } from 'react';
import { getMartyContext } from './ContextModule.js'

import './Child1.css';

const martyContext = getMartyContext()

export const Child1 = () => {
  let counter = useContext(martyContext)

  return (
    <div className="Child1">
        Child1: {counter}
    </div>
  )
}
