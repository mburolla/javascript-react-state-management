import React from 'react'
import { useContext } from 'react';
import { getMartyContext } from './ContextModule.js'

import './Child2.css';

const martyContext = getMartyContext()

export const Child2 = () => {
    let counter = useContext(martyContext)

    return (
        <div className='Child2'>
            Child2: {counter}
        </div>
    )
}
