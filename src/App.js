import { Child1 } from './Child1'
import { Child2 } from './Child2'
import { Child3 } from './Child3'
import { Child4 } from './Child4'
import { useState } from 'react'
import { useRecoilState } from 'recoil'
import { businessNameState } from './atoms'
import { getMartyContext } from './ContextModule.js'

import './App.css';

const MartyContext = getMartyContext();

function App() {
  let [counter, setCounter] = useState(0)
  const [businessName, setBusinessName] = useRecoilState(businessNameState)

  const onHandleClick = () => {
    setCounter(++counter)
  }

  const onHandleClick2 = () => {
    setBusinessName('Google')
  }

  return (
    <div className="App">
      <h3>UseContext</h3>
      <hr/>
      <MartyContext.Provider value={counter}>
          <button onClick={() => onHandleClick()}>Push</button>
          <br />
          { counter } 
          <Child1 />
          <Child2 />
      </MartyContext.Provider>
      <br/>
      <h3>Recoil</h3>
      <hr/>
      { businessName }
      <button onClick={() => onHandleClick2()}>Push</button>
      <Child3 />
      <Child4 />
    </div>
  );
}

export default App;
