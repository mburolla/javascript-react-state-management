import React from 'react'
import { useRecoilState } from 'recoil'
import { businessNameState } from './atoms'

import './Child3.css';

export const Child3 = () => {
    const [businessName, setBusinessName] = useRecoilState(businessNameState)

    const onHandleClick = () => {
        setBusinessName('Kodak')
    }

    return (
    <div className="Child3">
        Child3: { businessName } <button onClick={() => onHandleClick()}>Push</button>
    </div>
    )
}
